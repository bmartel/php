 <?php
session_start();
require 'connect.php';

define('TOTAL_ATTEMPTS', 5);
define('TIME_PASSED', 30);
define('LOCKOUT_TIME', 180);

if(isset($_COOKIE['lockout'])){
	$_SESSION['message'] = "<h3>YOU HAVE BEEN LOCKED OUT FOR 3 MINUTES</h3>
						<p>While you wait, how about watching a cat read a book. Isn't that amazing?</p>
						<img src=\"cat reading harry potter.jpg\" alt=\"cat reading harry potter\" />";	
}else{
	if(isset($_POST['submit'])){	
			if(sha1(strtoupper($_POST['verify_phrase'])) == $_SESSION['pass_phrase']){
				$_SESSION = array();
				$email = $_POST['email'];
				$comment = $_POST['comment'];
				
				$result = $db->query("INSERT INTO comments VALUES(NULL,'{$email}','{$comment}')");
				
				$_SESSION['message'] = '<h2>Success! you have inserted your comment to the table!</h2> <br />
									<p>Email: '.$email.'</p>
									<p>Comment: '.$comment.'</p>
									<a href="captcha_challenge.php">Try Again?</a>';
				setcookie("count_attempts", 0, 1); 
				setcookie("attempt_timer", 0, 1); 
			}
			else{
				if(!isset($_COOKIE['attempt_timer'])){
					$timer = time() + TIME_PASSED ;
					setcookie("attempt_timer", $timer, $timer); 
				}
				else{
					$timer = $_COOKIE['attempt_timer'];
				}
				if(!isset($_COOKIE['count_attempts'])){
					$attempts_remaining = 0;
					$count =0;
				}
				else{
					$count = $_COOKIE['count_attempts'];
					$count++;
				}
				
				setcookie("count_attempts", $count, time() + TIME_PASSED); 
				if($count >= TOTAL_ATTEMPTS - 1){
					$lockout = 1;
					setcookie("lockout", $lockout, time() + LOCKOUT_TIME); 
				}
				$_SESSION = array();
				$attempts_remaining = TOTAL_ATTEMPTS - $count;
				$time_remaining =  $timer - time();
				
				$_SESSION['message'] =  '<p>Number of attempts left ' .$attempts_remaining.' in the next '. $time_remaining.' seconds</p>
									<form action="captcha_challenge.php" method="post">
										<label for="email">Email:</label><br />
										<input type="text" id="email" name="email" value="'.$_POST['email'].'"/><br />
										<label for="comment">Comment:</label><br />
										<textarea name="comment" id="comment" rows="4" cols="28">'.$_POST['comment'].'</textarea><br />
										<img src="captcha.php" alt="Verify this Captcha!" /><br />
										<label for="verify_phrase">Verify Captcha:</label>
										<input type="text" id="verify_phrase" name="verify_phrase" /><br />
										<input type="submit" value="submit" name="submit" />
									</form>';
									
			}
		}
		else{
			$_SESSION['message'] =  '<h2>Add a Comment</h2>
							<form action="captcha_challenge.php" method="post">
								<label for="email">Email:</label><br />
								<input type="text" id="email" name="email" /><br />
								<label for="comment">Comment:</label><br />
								<textarea name="comment" id="comment" rows="4" cols="28"></textarea><br />
								<img src="captcha.php" alt="Verify this Captcha!" /><br />
								<label for="verify_phrase">Verify Captcha:</label>
								<input type="text" id="verify_phrase" name="verify_phrase" /><br />
								<input type="submit" value="submit" name="submit" />
							</form>';
		}
}
?>
<!doctype html>
<html>
<head>
		<title>Comments!</title>
</head>
<body>
	<?=$_SESSION['message']; ?>
</body>
</html>