<?php
	session_start();
	//	Set some important CAPTCHA Constants
	define('CAPTCHA_NUMCHARS', 6);	//# OF CHARS IN PASS PHRASE
	define('CAPTCHA_WIDTH', 250);	//WIDTH OF IMAGE
	define('CAPTCHA_HEIGHT', 125);	//HEIGHT OF IMAGE
	define('FONT_SIZE', 30);		//font size of captcha text

	//	Set MIME-type to PNG
	header("Content-type:image/png");

	//	Generate the random pass phrase
	$pass_phrase = "";
	for($i = 0;$i < CAPTCHA_NUMCHARS;$i++){
		$pass_phrase .= chr(rand(97,122));		//Using ASCII codes for pass phrase
	}
	$unmod_phrase = $pass_phrase;

	//	Store the encrypted pass phrase in a session variable
	$_SESSION['pass_phrase'] = sha1(strtoupper($pass_phrase));

	//	Create the image
	$image = imagecreatetruecolor(CAPTCHA_WIDTH, CAPTCHA_HEIGHT);

	//	Set a white background with red text and gray graphics
	$bg_color = imagecolorallocate($image, 255, 255, 255);
	$text_color = imagecolorallocate($image, 255, 0, 0);
	$graphic_color = imagecolorallocate($image, 64, 64, 64);

	// 	Fill the background
	//	Syntax: imagefilledrectangle(image, x1, y1, x2, y2, color)
	imagefilledrectangle($image, 0, 0, CAPTCHA_WIDTH, CAPTCHA_HEIGHT, $bg_color);

	//	Draw some random lines
	for($i = 0; $i < 5; $i++){
		//	Syntax: imageline(image, x1, y1, x2, y2, color)
		imageline($image, 0, rand() % CAPTCHA_HEIGHT, CAPTCHA_WIDTH, 
			rand() % CAPTCHA_HEIGHT, $graphic_color);
	}

	//	Sprinkle in some random dots
	for($i = 0; $i < 50; $i++){
		//	Syntax: imagesetpixel(image, x, y, color)
		imagesetpixel($image, rand() % CAPTCHA_WIDTH, rand() % CAPTCHA_HEIGHT, $graphic_color);
	}
	
	//	Draw the pass_phrase string
	//	Syntax: imagettftext(image, size, angle, x, y, color, fontfile, text)
	imagettftext($image, FONT_SIZE, 1, (CAPTCHA_WIDTH/2)-FONT_SIZE, CAPTCHA_HEIGHT/2, $text_color, 
		"inflammable age.ttf", $unmod_phrase);

	//	Output the image as a PNG
	imagepng($image);

	//	Clean up
	imagedestroy($image);
?>