<?php
	$date = date(DATE_RSS);
	$quotes = array("“Misfortune shows those who are not really friends.” - Aristotle",
					"“The only true wisdom is knowing that you know nothing.” - Socrates",
					"“Death is nothing, but to live defeated and inglorious is to die daily.” - Napoleon Bonaparte",
					"“You must be the change you wish to see in the world.” - Gandhi",
					"“By working faithfully eight hours a day you may eventually get to be boss and work twelve hours a day.” - Robert Frost",
					"“When angry, count to four; when very angry, swear.” - Mark Twain",
					"“Life’s tragedy is that we get old too soon and wise too late.” - Benjamin Franklin"
				   );
	$links = array(
		            array('title' => 'Stung Eye', 'href' => 'http://stungeye.com'),
		            array('title' => 'Glutton', 'href' => 'http://codeglutton.tumblr.com'),
		            array('title' => 'Reddit', 'href' => 'http://reddit.com' ),
        		  );
	$random_quote = $quotes[array_rand($quotes, 1)];
?>
<?='<?xml version="1.0" encoding="UTF-8"?>'?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
        <meta http-equiv="content-type" content="text/html;charset=utf-8" />
        <title>PHP Ultimate Challenge #2</title>
</head>
<body>
        <!-- This is where you will be writing and generating your XHTML. -->
        <h2>PHP Ultimate Challenge #2</h2>
        <?php echo "<p>" . $date . "</p>" ?>
        <?php echo "<p>" . $random_quote . "</p><ul>" ?>
        <?php foreach($links as $link){
        		echo '<li><a href="' . $link['href'] . '">' . $link['title'] . '</a></li>';
			   }
		?>
		<?php echo "</ul>"?>
		
</body>
</html>