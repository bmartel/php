<?php
$weather_wpg = array(
                    'location'               => 'Winnipeg, MB',
                    'current_date'           => '9 Jan 2009',
                    'current_conditions' => array(
                                                'temp' => -23,
                                                'text' => 'Sunny'),
                    'forecast'               => array(
                                                array(
                                                    'date' => '10 Jan 2009',
                                                    'high' => -17,
                                                    'low'  => -25),
                                                array(
                                                    'date' => '11 Jan 2009',
                                                    'high' => -14,
                                                    'low'  => -22)));
$weather_sun = array(
                    'location'               => 'Sunnyvale, CA',
                    'current_date'           => '9 Jan 2009',
                    'current_conditions' => array(
                                                'temp' => 15,
                                                'text' => 'Partly Cloudy'),
                    'forecast'               => array(
                                                array(
                                                    'date' => '10 Jan 2009',
                                                    'high' => 18,
                                                    'low'  => 12),
                                                array(
                                                    'date' => '11 Jan 2009',
                                                    'high' => 17,
                                                    'low'  => 14),
                                                array(
                                                    'date' => '12 Jan 2009',
                                                    'high' => 17,
                                                    'low'  => 12)));
													
function forecast_english($weather_forecast){
	
	$location = $weather_forecast['location'];
	$current_date = $weather_forecast['current_date'];
	$current_conditions = $weather_forecast['current_conditions'];
	$forecast_list = $weather_forecast['forecast'];
	$num_days = count($forecast_list);
	
	echo "The current forecast for {$location}, as of {$current_date} is {$current_conditions['text']} and {$current_conditions['temp']}<span>&deg;</span>C<br />";
	echo "<br />The forecast for the next {$num_days} days:<br />";
	
	foreach ($forecast_list as $forecast){
		echo "[{$forecast['date']}] HIGH: {$forecast['high']} LOW: {$forecast['low']}<br />";
	}
	
	echo"<br />";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Weather Forecast</title>
	<style>
		table{
			border: 1px #444 solid;
		}
		th{
			border: 1px #444 solid;
		}
		td{
			border: 1px #444 solid;
		}
	</style>
</head>
<body>
	<h1></h1>
	<p>
		Here is some fake weather forecasts brought to you by way of php!
	</p>
	
	<div>
		<?php forecast_english($weather_sun);?>
	</div>
	
	<div>
		<?php forecast_english($weather_wpg);?>
	</div>
</body>
</html>