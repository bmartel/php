	<?php
	
		function cigar_party($number_of_cigars, $is_weekend) {
			
			$success = FALSE;
			if($is_weekend && $number_of_cigars > 39){
				$success = TRUE;
			}
			if($number_of_cigars > 39 && $number_of_cigars < 61){
				$success = TRUE;
			}
			
			return $success;
		}
	
		// cigar_party(30, false);
		// // Should return boolean false.
		// cigar_party(50, false);
		// // Should return boolean true.
		// cigar_party(70, true);
		// // Should return boolean true.
	?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Cigar Party!</title>
	<style>
		table{
			border: 1px #444 solid;
		}
		th{
			border: 1px #444 solid;
		}
		td{
			border: 1px #444 solid;
		}
	</style>
</head>
<body>
	<h1>Some party indeed!</h1>
	<p>
		When squirrels get together for a party, they like to have cigars. 
		A squirrel party is successful when the number of cigars is between 40 and 60, inclusive. 
		Unless it is the weekend, in which case there is no upper bound on the number of cigars. 
	</p>
	<?php
		$success = "Yes";
		$notsuccess = "No";
		$weekend = FALSE;
		
		print "<table>";
		print "<tr><th>Number of Cigars</th><th>Is the Weekend?</th><th>Successful Party</th></tr>";
		for($i=0; $i < 10; $i++)
		{
			$cigar_count = rand(30, 100);
			if(rand(1,100) % 2 == 0)
			{
				$weekend = TRUE;
			}
			else {
				$weekend = FALSE;
			}
			
			print "<tr>";
			print "<td>" . $cigar_count . "</td>";
			if($weekend){
				print "<td>Yes</td>";
			}
			else {
				print "<td>No</td>";
			}
			if(cigar_party($cigar_count, $weekend)){
				print "<td>Yes</td>";
			}
			else{
				print "<td>No</td>";
			}
			print"</tr>";
		}
		
		print "</table>";
	?>
</body>
</html>